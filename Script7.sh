#!/bin/bash
echo "Path of the SSH key: "
read SSHKEY
echo "Path of the directory: "
read DIRECTORY
echo "Destination user: "
read USER
echo "Destination host: "
read DESTINATIONADDRESS
echo "Destination directory"
read DESTINATION


scp -rp -i $SSHKEY $DIRECTORY $USER@$DESTINATIONADDRESS:$DESTINATION